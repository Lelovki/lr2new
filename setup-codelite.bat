@echo off
set PATH=%PATH%;C:\MinGW-w64\mingw32\bin
if "x%ProgramFiles(x86)%"=="x" (
    set PATH=%PATH%;%ProgramFiles%\CMake\bin
) else (
    set PATH=%PATH%;%ProgramFiles(x86)\CMake\bin
)
cmake -G "CodeLite - MinGW Makefiles"

