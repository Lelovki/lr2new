#!/usr/bin/env sh
rm -rf \
    CMakeCache.txt CMakeFiles cmake_install.cmake Makefile \
    *.project *.workspace \
    *.cbp *.workspace *.layout

